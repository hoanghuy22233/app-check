import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

class AddContacts extends StatefulWidget {
  final FirebaseUser user;
  final String password;
  final bool wantsTouchId;
  AddContacts(
      {@required this.user, this.password, @required this.wantsTouchId});
  @override
  _AddContactsState createState() => _AddContactsState();
}

class _AddContactsState extends State<AddContacts> {
  TextEditingController _nameController, _numberController;
  String _typeSelected = '';
  final LocalAuthentication auth = LocalAuthentication();
  final storage = FlutterSecureStorage();
  CalendarController _controller;
  Map<DateTime, List<dynamic>> _events;
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  SharedPreferences prefs;

  DatabaseReference _ref;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.wantsTouchId) {
      authenticate();
    }
    _nameController = TextEditingController();
    _numberController = TextEditingController();
    _nameController.text = widget.user.email;
    _ref = FirebaseDatabase.instance.reference().child('Contacts');
    _controller = CalendarController();
    _eventController = TextEditingController();
    _events = {};
    _selectedEvents = [];
    initPrefs();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      _events = Map<DateTime, List<dynamic>>.from(
          decodeMap(json.decode(prefs.getString("events") ?? "{}")));
    });
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  void authenticate() async {
    final canCheck = await auth.canCheckBiometrics;
    List<BiometricType> list = List();
    if (canCheck) {
      list = await auth.getAvailableBiometrics();

      if (Platform.isAndroid) {
        if (list.contains(BiometricType.face)) {
          // Face ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        } else if (list.contains(BiometricType.fingerprint)) {
          // Touch ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        }
      }
    } else {
      print('cant check');
    }
  }

  Widget _buildContactType(String title) {
    return InkWell(
      child: Container(
        height: 40,
        width: 90,
        decoration: BoxDecoration(
          color: _typeSelected == title
              ? Colors.green
              : Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
      onTap: () {
        setState(() {
          _typeSelected = title;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('chấm công'),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TableCalendar(
                events: _events,
                initialCalendarFormat: CalendarFormat.month,
                calendarStyle: CalendarStyle(
                    canEventMarkersOverflow: true,
                    todayColor: Colors.orange,
                    selectedColor: Colors.red,
                    todayStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        color: Colors.white)),
                headerStyle: HeaderStyle(
                  centerHeaderTitle: true,
                  formatButtonDecoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  formatButtonTextStyle: TextStyle(color: Colors.white),
                  formatButtonShowsNext: false,
                ),
                startingDayOfWeek: StartingDayOfWeek.monday,
                onDaySelected: (DateTime day, List events, List holidays) {
                  setState(() {
                    _selectedEvents = events;
                    String name = widget?.user?.email ?? _nameController.text;
                    // String number = _numberController.text;

                    Map<String, String> contact = {
                      'name': name,
                      // 'number': number,
                      'type': DateTime.now().toString(),
                    };
                    _ref.push().set(contact).then((value) {
                      Navigator.pop(context);
                    });
                  });
                },
                builders: CalendarBuilders(
                  selectedDayBuilder: (context, date, events) => Container(
                      margin: const EdgeInsets.all(4.0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Text(
                        date.day.toString(),
                        style: TextStyle(color: Colors.white),
                      )),
                  todayDayBuilder: (context, date, events) => Container(
                      margin: const EdgeInsets.all(4.0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Text(
                        date.day.toString(),
                        style: TextStyle(color: Colors.white),
                      )),
                ),
                calendarController: _controller,
              ),
              ..._selectedEvents.map((event) => Column(
                    children: [
                      ListTile(
                        leading: Image.asset(
                          "assets/images/image_logo.png",
                          scale: 1.5,
                        ),
                        title: Text(
                          event,
                          style: TextStyle(color: Colors.blue),
                        ),
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        trailing: GestureDetector(
                          onTap: () {
                            setState(() {
                              _events[_controller.selectedDay].remove(event);
                              prefs.setString(
                                  "events", json.encode(encodeMap(_events)));
                              _eventController.clear();
                            });
                          },
                          child: Icon(
                            Icons.delete,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Divider(
                          color: Colors.grey,
                          height: 10,
                        ),
                      )
                    ],
                  )),
              // Container(
              //   width: double.infinity,
              //   padding: EdgeInsets.symmetric(horizontal: 10),
              //   child: RaisedButton(
              //     child: Text(
              //       'Check List',
              //       style: TextStyle(
              //         fontSize: 20,
              //         color: Colors.red,
              //         fontWeight: FontWeight.w600,
              //       ),
              //     ),
              //     onPressed: () {
              //       saveContact();
              //     },
              //     color: Colors.white,
              //   ),
              // ),
            ],
          ),
        ));
  }

  void saveContact() {
    String name = widget?.user?.email ?? _nameController.text;
    // String number = _numberController.text;

    Map<String, String> contact = {
      'name': name,
      // 'number': number,
      'type': _selectedEvents.toString(),
    };

    _ref.push().set(contact).then((value) {
      Navigator.pop(context);
    });
  }
}
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_picker/flutter_picker.dart';
//
// class CreateNewNote extends StatefulWidget {
//   CreateNewNote({Key key, this.title}) : super(key: key);
//   final String title;
//   @override
//   _CreateNewNoteState createState() => _CreateNewNoteState();
// }
//
// class _CreateNewNoteState extends State<CreateNewNote> {
//   Icon searchIcon = new Icon(Icons.search);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         resizeToAvoidBottomPadding: true,
//         body: Stack(
//           alignment: Alignment.center,
//           children: <Widget>[
//             Positioned(
//               top: 0,
//               left: 0,
//               right: 0,
//               child: Container(
//                 padding: EdgeInsets.all(20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     border: InputBorder.none,
//                     labelText: 'Tiêu đề',
//                     alignLabelWithHint: true,
//                   ),
//                   style: TextStyle(fontSize: 20.0, color: Colors.black),
//                   keyboardType: TextInputType.multiline,
//                   maxLines: 2,
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 80,
//               left: 0,
//               right: 0,
//               child: Container(
//                 padding: EdgeInsets.all(20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     border: InputBorder.none,
//                     labelText: 'Nhập ghi chú',
//                     alignLabelWithHint: true,
//                   ),
//                   style: TextStyle(fontSize: 18.0, color: Colors.black),
//                   keyboardType: TextInputType.multiline,
//                   maxLines: 12,
//                 ),
//               ),
//             ),
//             Positioned(
//               left: 20,
//               bottom: 20,
//               child: FloatingActionButton(
//                 onPressed: () {
//                   showPickerDateCustom(context);
//                 },
//                 child: Icon(
//                   Icons.alarm,
//                   color: Colors.white,
//                 ),
//               ),
//             ),
//           ],
//         )
//         // This trailing comma makes auto-formatting nicer for build methods.
//         );
//   }
//
//   showPickerDateCustom(BuildContext context) {
//     new Picker(
//         hideHeader: true,
//         adapter: new DateTimePickerAdapter(
//           minValue: DateTime.now(),
//           customColumnType: [2, 1, 0, 3, 4],
//         ),
//         title: new Text("Chọn thời gian"),
//         selectedTextStyle: TextStyle(color: Colors.blue),
//         onConfirm: (Picker picker, List value) {
//           print((picker.adapter as DateTimePickerAdapter).value);
//         }).showDialog(context);
//   }
// }
