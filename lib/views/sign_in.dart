import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_chat/dialog/loading_dialog.dart';
import 'package:test_chat/helper/helperfunctions.dart';
import 'package:test_chat/services/database.dart';

import '../sharedPrefs.dart';
import 'contacts.dart';

class SignIn extends StatefulWidget {
  final Function cancelBackToHome;
  final Function toggleView;
  SignIn({this.cancelBackToHome, this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final LocalAuthentication auth = LocalAuthentication();
  final FlutterSecureStorage storage = FlutterSecureStorage();
  TextEditingController emailEditingController = new TextEditingController();
  TextEditingController passwordEditingController = new TextEditingController();
  bool userHasTouchId = false;
  String email, password;
  bool _useTouchId = false;

  final formKey = GlobalKey<FormState>();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getSecureStorage();
    _getSharedPrefsPass();
    _getSharedPrefs();
    var now = DateTime.now();
    var startTime = DateTime(now.year, now.month, now.day, 08, 30); // eg 7 AM
    var endTime = DateTime(now.year, now.month, now.day, 21, 35); // eg 10 PM
    setStartTime(startTime);
    setEndTime(endTime);
  }

  void getSecureStorage() async {
    final isUsingBio = await storage.read(key: 'usingBiometric');
    setState(() {
      userHasTouchId = isUsingBio == 'true';
    });
  }

  void authenticate() async {
    final canCheck = await auth.canCheckBiometrics;
    LoadingDialog.showLoadingDialog(context, "Loading...");
    List<BiometricType> list = List();
    if (canCheck) {
      list = await auth.getAvailableBiometrics();

      if (Platform.isAndroid) {
        if (list.contains(BiometricType.face)) {
          // Face ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            final userStoredEmail = await storage.read(key: 'email');
            final userStoredPassword = await storage.read(key: 'password');

            _signIn(em: userStoredEmail, pw: userStoredPassword);
            LoadingDialog.hideLoadingDialog(context);
          }
        } else if (list.contains(BiometricType.fingerprint)) {
          // Touch ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            final userStoredEmail = await storage.read(key: 'email');
            final userStoredPassword = await storage.read(key: 'password');
            _signIn(em: userStoredEmail, pw: userStoredPassword);
            LoadingDialog.hideLoadingDialog(context);
          }
        }
      }
    } else {
      print('cant check');
    }
  }

  void _signIn({String em, String pw}) {
    LoadingDialog.showLoadingDialog(context, "Loading...");
    _auth
        .signInWithEmailAndPassword(email: em, password: pw)
        .then((authResult) async {
      QuerySnapshot userInfoSnapshot =
          await DatabaseMethods().getUserInfo(email);
      _saveAccount(account: emailEditingController.text);
      _savePass(pass: passwordEditingController.text);
      HelperFunctions.saveUserLoggedInSharedPreference(true);
      HelperFunctions.saveUserNameSharedPreference(
          userInfoSnapshot.documents[0].data["userName"]);
      HelperFunctions.saveUserEmailSharedPreference(
          userInfoSnapshot.documents[0].data["userEmail"]);
      LoadingDialog.hideLoadingDialog(context);

      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return Contacts(
          user: authResult.user,
          wantsTouchId: _useTouchId,
          password: password,
        );
      }));
    }).catchError((err) {
      print(err.code);
      if (err.code == 'ERROR_WRONG_PASSWORD') {
        LoadingDialog.hideLoadingDialog(context);
        showCupertinoDialog(
            context: context,
            builder: (context) {
              return CupertinoAlertDialog(
                title: Text('Sai mật khẩu, vui lòng kiểm tra lại'),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
      } else if (err.code == 'ERROR_USER_NOT_FOUND') {
        LoadingDialog.hideLoadingDialog(context);
        showCupertinoDialog(
            context: context,
            builder: (context) {
              return CupertinoAlertDialog(
                title: Text('Sai Email, vui lòng kiểm tra lại'),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
      }
    });
  }
  // void _signIn({String em, String pw}) {
  //   _auth
  //       .signInWithEmailAndPassword(email: em, password: pw)
  //       .then((authResult) async {
  //     if (authResult != null) {
  //       QuerySnapshot userInfoSnapshot =
  //           await DatabaseMethods().getUserInfo(email);
  //
  //       HelperFunctions.saveUserLoggedInSharedPreference(true);
  //       HelperFunctions.saveUserNameSharedPreference(
  //           userInfoSnapshot.documents[0].data["userName"]);
  //       HelperFunctions.saveUserEmailSharedPreference(
  //           userInfoSnapshot.documents[0].data["userEmail"]);
  //
  //       Navigator.pushReplacement(context,
  //           MaterialPageRoute(builder: (context) {
  //         return ChatRoom(
  //           user: authResult.user,
  //           wantsTouchId: _useTouchId,
  //           password: password,
  //         );
  //       }));
  //     } else {
  //       setState(() {
  //         isLoading = false;
  //         //show snackbar
  //       });
  //     }
  //   }).catchError((err) {
  //     print(err.code);
  //     if (err.code == 'ERROR_WRONG_PASSWORD') {
  //       showCupertinoDialog(
  //           context: context,
  //           builder: (context) {
  //             return CupertinoAlertDialog(
  //               title: Text('The password was incorrect, please try again'),
  //               actions: <Widget>[
  //                 CupertinoDialogAction(
  //                   child: Text('OK'),
  //                   onPressed: () {
  //                     Navigator.pop(context);
  //                   },
  //                 )
  //               ],
  //             );
  //           });
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            children: <Widget>[
              Text(
                'Đăng nhập',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                controller: emailEditingController,
                onChanged: (textVal) {
                  setState(() {
                    email = textVal;
                  });
                },
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                  )),
                  hintText: 'Email',
                  hintStyle: TextStyle(color: Colors.white.withOpacity(0.6)),
                  focusColor: Colors.white,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                  ),
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                onChanged: (textVal) {
                  setState(() {
                    password = textVal;
                  });
                },
                obscureText: true,
                controller: passwordEditingController,
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                  )),
                  hintText: 'Mật khẩu',
                  hintStyle: TextStyle(color: Colors.white.withOpacity(0.6)),
                  focusColor: Colors.white,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                  ),
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              userHasTouchId
                  ? InkWell(
                      onTap: () {
                        authenticate();
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          padding: EdgeInsets.all(10.0),
                          child: Icon(
                            FontAwesomeIcons.fingerprint,
                            size: 30,
                          )),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularCheckBox(
                          checkColor: Colors.blue[200],
                          activeColor: Colors.white,
                          inactiveColor: Colors.white,
                          disabledColor: Colors.grey,
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          value: _useTouchId,
                          onChanged: (newValue) {
                            setState(() {
                              _useTouchId = newValue;
                            });
                          },
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          'Use Touch ID',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                          ),
                        )
                      ],
                    ),
              SizedBox(
                height: 12.0,
              ),
              InkWell(
                  onTap: () {
                    _signIn(em: email, pw: password);
                    // _saveAccount(account: email);
                    // _savePass(pass: password);
                    // setState(() {
                    //   isLoading =true;
                    // });
                  },
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(
                      vertical: 16.0,
                      horizontal: 34.0,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        30.0,
                      ),
                    ),
                    child: Text(
                      'Đăng nhập',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(30.0),
          color: Colors.black.withOpacity(0.2),
          child: GestureDetector(
            onTap: () {
              widget.cancelBackToHome();
            },
            child: Text(
              'Bạn chưa có tài khoản? Đăng ký',
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ],
    );
  }

  Future<Null> _saveAccount({String account}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(HelperFunctions.sharedPreferenceUserEmailKey, account);
  }

  Future<Null> _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    emailEditingController.text =
        email = prefs.getString(HelperFunctions.sharedPreferenceUserEmailKey);
  }

  Future<Null> _savePass({String pass}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(HelperFunctions.sharedPreferenceUserPassKey, pass);
  }

  Future<Null> _getSharedPrefsPass() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    passwordEditingController.text =
        password = prefs.getString(HelperFunctions.sharedPreferenceUserPassKey);
    print("_______________");
    print(password);
  }
}
