import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:local_auth/local_auth.dart';
import 'package:test_chat/services/auth.dart';
import 'package:test_chat/views/add_contacts.dart';

import '../customizedNotification.dart';
import '../sharedPrefs.dart';
import 'menu_frame.dart';

class Contacts extends StatefulWidget {
  final FirebaseUser user;
  final String password;
  final bool wantsTouchId;
  Contacts({@required this.user, this.password, this.wantsTouchId});
  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  Query _ref;
  final LocalAuthentication auth = LocalAuthentication();
  final storage = FlutterSecureStorage();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var now = DateTime.now();
    var startTime = DateTime(now.year, now.month, now.day, 08, 30); // eg 7 AM
    var endTime = DateTime(now.year, now.month, now.day, 21, 35); // eg 10 PM
    setStartTime(startTime);
    setEndTime(endTime);
    if (widget.wantsTouchId) {
      authenticate();
    }
    _ref = FirebaseDatabase.instance
        .reference()
        .child('Contacts')
        .orderByChild('name');
  }

  void authenticate() async {
    final canCheck = await auth.canCheckBiometrics;
    List<BiometricType> list = List();
    if (canCheck) {
      list = await auth.getAvailableBiometrics();

      if (Platform.isAndroid) {
        if (list.contains(BiometricType.face)) {
          // Face ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        } else if (list.contains(BiometricType.fingerprint)) {
          // Touch ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        }
      }
    } else {
      print('cant check');
    }
  }

  Widget _buildContactItem({Map contact}) {
    Color typeColor = getTypeColor(contact['type']);
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.all(10),
      height: 100,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(
                Icons.person,
                color: Theme.of(context).primaryColor,
                size: 20,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                contact['name'],
                style: TextStyle(
                    fontSize: 16,
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              // Icon(
              //   Icons.phone_iphone,
              //   color: Theme.of(context).accentColor,
              //   size: 20,
              // ),
              // SizedBox(
              //   width: 6,
              // ),
              // Text(
              //   contact['number'],
              //   style: TextStyle(
              //       fontSize: 16,
              //       color: Theme.of(context).accentColor,
              //       fontWeight: FontWeight.w600),
              // ),
              // SizedBox(width: 15),
              Icon(
                Icons.group_work,
                color: typeColor,
                size: 20,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                contact['type'],
                style: TextStyle(
                    fontSize: 16,
                    color: typeColor,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context, {Map contact}) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/images/image_logo.png",
          height: 40,
        ),
        elevation: 0.0,
        centerTitle: false,
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return CustomizedNotification();
              }));
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(
                  Icons.alarm,
                  color: Colors.white,
                )),
          ),
          GestureDetector(
            onTap: () {
              FirebaseDatabase.instance.reference().child('Contacts').remove();
              print("____________");
              print(FirebaseDatabase.instance.reference().child('Contacts'));
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                )),
          ),
          GestureDetector(
            onTap: () {
              AuthService().signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => MenuFrame()));
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(Icons.exit_to_app)),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        child: FirebaseAnimatedList(
          query: _ref,
          itemBuilder: (BuildContext context, DataSnapshot snapshot,
              Animation<double> animation, int index) {
            Map contact = snapshot.value;
            return _buildContactItem(contact: contact);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (_) {
              return AddContacts(
                user: widget.user,
                wantsTouchId: widget.wantsTouchId,
                password: widget.password,
              );
            }),
          );
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
    );
  }

  Color getTypeColor(String type) {
    Color color = Theme.of(context).accentColor;

    if (type == 'Work') {
      color = Colors.brown;
    }

    if (type == 'Family') {
      color = Colors.green;
    }

    if (type == 'Friends') {
      color = Colors.teal;
    }
    return color;
  }
}
