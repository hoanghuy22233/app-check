import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:local_auth/local_auth.dart';
import 'package:test_chat/views/chatrooms.dart';
import 'package:test_chat/views/contacts.dart';

class SignedInPage extends StatefulWidget {
  final FirebaseUser user;
  final bool wantsTouchId;
  final String password;

  SignedInPage(
      {@required this.user, @required this.wantsTouchId, this.password});

  @override
  _SignedInPageState createState() => _SignedInPageState();
}

class _SignedInPageState extends State<SignedInPage>
    with SingleTickerProviderStateMixin {
  final LocalAuthentication auth = LocalAuthentication();
  final storage = FlutterSecureStorage();
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    if (widget.wantsTouchId) {
      authenticate();
    }
    _tabController = new TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  void authenticate() async {
    final canCheck = await auth.canCheckBiometrics;
    List<BiometricType> list = List();
    if (canCheck) {
      list = await auth.getAvailableBiometrics();

      if (Platform.isAndroid) {
        if (list.contains(BiometricType.face)) {
          // Face ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        } else if (list.contains(BiometricType.fingerprint)) {
          // Touch ID.
          final authenticated = await auth.authenticateWithBiometrics(
              localizedReason: 'Enable Face ID to sign in more easily');
          if (authenticated) {
            storage.write(key: 'email', value: widget.user.email);
            storage.write(key: 'password', value: widget.password);
            storage.write(key: 'usingBiometric', value: 'true');
          }
        }
      }
    } else {
      print('cant check');
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Column(
          children: [
            _buildTabBarMenu(),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  ChatRoom(),
                  Contacts(
                    user: widget.user,
                    password: widget.password,
                    wantsTouchId: widget.wantsTouchId,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTabBarMenu() {
    return new Container(
      // decoration: BoxDecoration(
      //     border: Border.all(color: AppColor.BLACK),
      //     borderRadius: BorderRadius.circular(500)),
      //  height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Chat",
          ),
          Tab(
            text: "List",
          ),
        ],
        // labelStyle: AppStyle.DEFAULT_SMALL,
        // unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: Colors.black,
      ),
    );
  }
}
