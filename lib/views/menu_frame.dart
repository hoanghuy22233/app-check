import 'package:flutter/material.dart';
import 'package:test_chat/views/sign_in.dart';
import 'package:test_chat/views/signup.dart';

import 'home_sign_in.dart';

class MenuFrame extends StatelessWidget {
  PageController pageController = PageController();
  Duration _animationDuration = Duration(milliseconds: 500);
  // Function showSignIn;
  // MenuFrame({this.showSignIn});

  void _changePage(int page) {
    pageController.animateToPage(page,
        duration: _animationDuration, curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Material(
          color: Colors.transparent,
          child: Container(
            color: Colors.black,
            child: Column(
              children: <Widget>[
                SafeArea(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 28.0, vertical: 40.0),
                    child: Image.asset("assets/images/image_logo.png"),
                  ),
                ),
                Expanded(
                  child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: pageController,
                    children: <Widget>[
                      HomeSignInWidget(
                        goToPageCallback: (page) {
                          _changePage(page);
                        },
                      ),
                      SignIn(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      SignUp(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
//            decoration: BoxDecoration(
//              gradient: LinearGradient(
//                begin: Alignment.topCenter,
//                end: Alignment.bottomCenter,
//                colors: [
//                  Color.fromRGBO(255, 123, 67, 1.0),
//                  Color.fromRGBO(245, 50, 111, 1.0),
//                ],
          ),
//            ),
//          ),
        ),
      ],
    );
  }
}
