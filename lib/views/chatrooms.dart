import 'package:flutter/material.dart';
import 'package:test_chat/helper/constants.dart';
import 'package:test_chat/helper/helperfunctions.dart';
import 'package:test_chat/helper/theme.dart';
import 'package:test_chat/services/database.dart';
import 'package:test_chat/views/chat.dart';
import 'package:test_chat/views/search.dart';

class ChatRoom extends StatefulWidget {
  // final FirebaseUser user;
  // final bool wantsTouchId;
  // final String password;
  // ChatRoom({@required this.user, @required this.wantsTouchId, this.password});
  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  Stream chatRooms;

  // final LocalAuthentication auth = LocalAuthentication();
  // final storage = FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    getUserInfogetChats();
    // if (widget.wantsTouchId) {
    //   authenticate();
    // }
  }

  // void authenticate() async {
  //   final canCheck = await auth.canCheckBiometrics;
  //   List<BiometricType> list = List();
  //   if (canCheck) {
  //     list = await auth.getAvailableBiometrics();
  //
  //     if (Platform.isAndroid) {
  //       if (list.contains(BiometricType.face)) {
  //         // Face ID.
  //         final authenticated = await auth.authenticateWithBiometrics(
  //             localizedReason: 'Enable Face ID to sign in more easily');
  //         if (authenticated) {
  //           storage.write(key: 'email', value: widget.user.email);
  //           storage.write(key: 'password', value: widget.password);
  //           storage.write(key: 'usingBiometric', value: 'true');
  //         }
  //       } else if (list.contains(BiometricType.fingerprint)) {
  //         // Touch ID.
  //         final authenticated = await auth.authenticateWithBiometrics(
  //             localizedReason: 'Enable Face ID to sign in more easily');
  //         if (authenticated) {
  //           storage.write(key: 'email', value: widget.user.email);
  //           storage.write(key: 'password', value: widget.password);
  //           storage.write(key: 'usingBiometric', value: 'true');
  //         }
  //       }
  //     }
  //   } else {
  //     print('cant check');
  //   }
  // }

  Widget chatRoomsList() {
    return StreamBuilder(
      stream: chatRooms,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                itemCount: snapshot.data.documents.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ChatRoomsTile(
                    userName: snapshot.data.documents[index].data['chatRoomId']
                        .toString()
                        .replaceAll("_", "")
                        .replaceAll(Constants.myName, ""),
                    chatRoomId:
                        snapshot.data.documents[index].data["chatRoomId"],
                  );
                })
            : Container();
      },
    );
  }

  getUserInfogetChats() async {
    Constants.myName = await HelperFunctions.getUserNameSharedPreference();
    DatabaseMethods().getUserChats(Constants.myName).then((snapshots) {
      setState(() {
        chatRooms = snapshots;
        print(
            "we got the data + ${chatRooms.toString()} this is name  ${Constants.myName}");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: chatRoomsList(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Search()));
        },
      ),
    );
  }
}

class ChatRoomsTile extends StatelessWidget {
  final String userName;
  final String chatRoomId;

  ChatRoomsTile({this.userName, @required this.chatRoomId});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Chat(
                      chatRoomId: chatRoomId,
                    )));
      },
      child: Container(
        color: Colors.black26,
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
        child: Row(
          children: [
            Container(
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                  color: CustomTheme.colorAccent,
                  borderRadius: BorderRadius.circular(30)),
              child: Text(userName.substring(0, 1),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'OverpassRegular',
                      fontWeight: FontWeight.w300)),
            ),
            SizedBox(
              width: 12,
            ),
            Text(userName,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: 'OverpassRegular',
                    fontWeight: FontWeight.w300))
          ],
        ),
      ),
    );
  }
}
