import 'package:flutter/material.dart';
import 'package:test_chat/helper/helperfunctions.dart';
import 'package:test_chat/views/menu_frame.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool userIsLoggedIn;

  @override
  void initState() {
    getLoggedInState();
    super.initState();
  }

  getLoggedInState() async {
    await HelperFunctions.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterChat',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.black,
        scaffoldBackgroundColor: Colors.blueGrey,
        accentColor: Colors.redAccent,
        fontFamily: "OverpassRegular",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MenuFrame(),
      //   home: userIsLoggedIn != null
      //       ? userIsLoggedIn
      //           ? SignedInPage()
      //           : Authenticate()
      //       : Container(
      //           child: Center(
      //             child: Authenticate(),
      //           ),
      //         ),
    );
  }
}
